# T00 - HTML + CSS

This is the repo for the webpage that was developed during the first lecture on 09/03/2023

  

## Content

The project consists in:

 - Four webpages (in the root folder):
	 - The homepage (index.html)
	 - The page containing the cards of the dogs (dogs.html)
	 - The about page (about.html)
	 - The contact page (contact.html)
 - CSS files (in folder assets/css):
	 - one for each webpage, using the same name
	 - one for the general CSS style shared between pages (general.css)
 - Images used in the webpages (in folder assets/img)
